// Macro to zero all except the least significant non-zero bit
#define LSB(i) ((i) & -(i)) 

// Fen_sum: returns the sum of elements from index 0 to index i
int Fen_sum(int *a, int i) {
  int sum = 0.0;
  i++;
  while (i > 0) {
	sum += a[i-1];
	i -= LSB(i);
  }
  return sum;
}

// Fen_add: adds k to element i
void Fen_add(int *a, int size, int k, int i) {
  i++;
  size++;
  while (i < size) {
	a[i-1] += k;
	i += LSB(i);
  }
}

// Fen_get: Returns the value of the element at index i
int Fen_get(int *a, int i) {
  return Fen_sum(a,i)-Fen_sum(a,i-1);
}

// Fen_set: sets the value of the element at index i
void Fen_set(int *a, int size, int value, int i) {
  Fen_add(a,size,value-Fen_get(a,i),i);
}

// Fen_add_element: adds a new element
void Fen_add_element(int *a, int size, int value) {
  size++;
  int i = size;
  a[size-1] = value + Fen_sum(a, size-2);

  while (i > 0) {
	i -= LSB(i);
	a[size-1] -= a[i-1];
  }
}
