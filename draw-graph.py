import numpy as np
#import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import re

n = int(sys.argv[1])
d = int(sys.argv[2])

output = './image_result'
if not os.path.exists(output):
    os.mkdir(output)

filename = "n%d_d%d_list" % (n, d)
count = 0
data_1 = None
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("./output/" + f) as textFile:
                for line in textFile:
                    count += 1
                    raw = [float(e) for e in line.split()]
                    if data_1 is None:
                        data_1 = raw
                    else:
                        data_1 = np.add(data_1, raw)
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)

if count > 1:
    data_1 = np.multiply(data_1, 1.0/count)
'''
filename = "n%d_d%d_goldberg" % (n, d)
count = 0
data_2 = None
for f in os.listdir("./output"):
    if f.find(filename) > -1:
        try:
            with open("./output/" + f) as textFile:
                for line in textFile:
                    count += 1
                    raw = [float(e) for e in line.split()]
                    if data_2 is None:
                        data_2 = raw
                    else:
                        data_2 = np.add(data_2, raw)
        except IOError as e:
            print "I/O error({0}): {1}".format(e.errno, e.strerror)

if count > 1:
    data_2 = np.multiply(data_2, 1.0/count)

exact_density = [d-d*d*1.0/k for k in xrange(n/len(data_1), n+1, n/len(data_1))]
'''
plt.figure(figsize=(24, 14))

x_axis = xrange(n/len(data_1), n+1, n/len(data_1))
plt.subplot(121)
plt.plot(x_axis, data_1)
#plt.plot(x_axis, data_2, 'o')
#plt.plot(x_axis, exact_density, 'x')
plt.legend(['d = %d' % d], loc='upper left')

plt.xlabel('# nodes')
plt.ylabel('max clique size')
plt.grid(True)
#plt.axis([1, 200, -1, 300])
plt.title('Linear scaling')

plt.subplot(122)
plt.loglog(x_axis, data_1)
plt.grid(True)
plt.title('Log-log scaling')

#plt.savefig(output + '/maxclique_n%d_d%d.png' % (n,d))
plt.show()
