#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <dirent.h>

using namespace std;
const string output_folder = "output";

const int n_first_nodes = 1000;

#include "mcqd/mcqd.h"
#include "fenwick-tree.h"

// add a new edge
void addEdge(int u, int v, bool **conn) {
  if (u < n_first_nodes && v < n_first_nodes) {
	conn[u][v] = true;
	conn[v][u] = true;
  }
}

//--------------------------------------------------------------------
/// Generate Barabasi-Albert model graph
/// and calculate the size of maximum clique of first 1000 nodes
/// @param n, d: parameters of Random Graph
/// @param increment: frequency of running maxclique algorithm
//--------------------------------------------------------------------
int *barabasi_albert_model(int n, int d, int increment) {
  int *result = new int[n/increment];
  int counter = 0;

  bool **conn = new bool *[n_first_nodes];
  for (int i = 0; i < n_first_nodes; i++) {
	conn[i] = new bool[n_first_nodes];
	for (int j = 0; j < n_first_nodes; j++)
	  conn[i][j] = false;
  }

  int *fenwick_array = new int[n]; // store Fenwick Tree 
  int *degrees = new int[n]; // store degree of each vertex
  bool *is_selected = new bool[n];
  for (int i = 0; i < n; i++)
	is_selected[i] = false;

  int *selected_vertice = new int[d];

  for (int i = 0; i < d; i++) {
	degrees[i] = 1;

	Fen_add_element(fenwick_array, i, 1);

    addEdge(i, d, conn);
	for (int j = 0; j < i; j++) {
	  addEdge(i, j, conn);
	}
  }
  degrees[d] = d;
  Fen_add_element(fenwick_array, d, d);

  int num_links = d;
  for (int i = d+1; i < n; i++) {
	int range = 2*num_links;

	// link new node to d old nodes
	for (int j = 0; j < d; j++) {
	  int random = rand() % range;
#if defined DEBUG
	  cout << "Random = " << random << endl;
#endif
	  int selected_vertex;

	  /*
	  int cumulative = 0;
	  for (selected_vertex = 0; selected_vertex < i; selected_vertex++) {
		if (is_selected[ selected_vertex ])
		  continue;
		cumulative += degrees[ selected_vertex ];
		if (random < cumulative)
		  break;
	  }
	  */
	  int min = 0;
	  int max = i;
	  while (min <= max) {
		selected_vertex = (min + max) / 2;
		int temp1 = Fen_sum(fenwick_array, selected_vertex-1);
		int temp2 = Fen_sum(fenwick_array, selected_vertex);
		if (random >= temp1 && random < temp2)
		  break;
		else if (random >= temp2)
		  min = selected_vertex + 1;
		else // if (random < temp1)
		  max = selected_vertex - 1;
	  }
#if defined DEBUG
	  cout << "Selected_Vertex = " << selected_vertex << endl;
#endif
	  
	  selected_vertice[j] = selected_vertex;
	  is_selected[ selected_vertex ] = true;
	  
	  range -= degrees[ selected_vertex ];
	  Fen_add(fenwick_array, i, -degrees[ selected_vertex ], selected_vertex);
	}
	
	// update degree of selected vertice
	for (int j = 0; j < d; j++) {
	  degrees[ selected_vertice[j] ]++;
	  is_selected[ selected_vertice[j] ] = false;

	  Fen_add(fenwick_array, i, degrees[ selected_vertice[j] ], selected_vertice[j]);

	  addEdge(selected_vertice[j], i, conn);

	  for (int k = 0; k < j; k++) {
	    addEdge(selected_vertice[j], selected_vertice[k], conn);
	  }
	}
	degrees[i] = d;
	Fen_add_element(fenwick_array, i, d);

	num_links += d;

	// run maximum clique algorithm
	if ((i+1) % increment == 0) {
	  Maxclique m(conn, n_first_nodes);
	  int *qmax, qsize;
	  m.mcqdyn(qmax, qsize);
	  result[ counter++ ] = qsize;
	  delete [] qmax;
	}
  }

  delete [] degrees;
  delete [] fenwick_array;
  delete [] selected_vertice;
  delete [] is_selected;

  for (int i = 0; i < n_first_nodes; i++)
	delete [] conn[i];
  delete conn;
  return result;
}

int getOutputFileNumber(int n, int d) {
  DIR *dir;
  dirent *pdir;
  int t_max = 0;

  dir = opendir(("./" + output_folder).c_str());
  int n_temp, d_temp, t_temp;
  string filename_temp;
  while (pdir = readdir(dir)) {
	filename_temp = pdir->d_name;
	n_temp = d_temp = t_temp = 0;
	sscanf(filename_temp.c_str(), "n%d_d%d_estimation_%d", &n_temp, &d_temp, &t_temp);
	if (n == n_temp && d == d_temp)
	  if (t_temp > t_max) {
		t_max = t_temp;
	  }
  }
  return t_max+1;
}

//--------------------------------------------------------------------
/// Process parameters
/// @param argc, argv: input parameters
/// @param n, d: parameters of Random Graph
/// @param t: the number of tries
/// @return true if successful
///         false otherwise
//--------------------------------------------------------------------
bool processInput(int argc, char *argv[], int &n, int &d,
				  int &increment, int &t) {
  if (argc >= 4) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return false;
    istringstream iss2( argv[2] );
    if (!(iss2 >> d))
      return false;
    istringstream iss3( argv[3] );
    if (!(iss3 >> increment))
      return false;
  }
  else {
	return false;
  }
  if (argc >= 5) {
	istringstream iss4( argv[4] );
    if (!(iss4 >> t))
	  return false;
  }
  return true;
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  int n, d, increment, tries = 1000;
  // process input
  if (processInput(argc, argv, n, d, increment, tries) == false)
	return 0;
  
  int fileNumber = getOutputFileNumber(n, d);
  fstream fs;
  char filename[128];
  snprintf(filename, sizeof(filename), "n%d_d%d_estimation_%d.txt", n, d, fileNumber);
  fs.open((output_folder + "/" + filename).c_str(), fstream::out | fstream::app);
  
  for (int i = 0; i < tries; i++) {
	int *result = barabasi_albert_model(n, d, increment);
	for (int j = 0; j < n/increment; j++)
	  fs << result[j] << " ";
	fs << endl;
	delete [] result;
  }
  
  fs.close();
  
  return 0;
}
