#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <dirent.h>
#include <set>
#include <list>
#include <vector>
#include <algorithm>
#include "fenwick-tree.h"

using namespace std;
const string output_folder = "output";

#ifdef ARRAY
#include "mcqd/mcqd.h"
#else
typedef vector< pair<int, int> > vertices; // (degree|color, label) of a vertex

//--------------------------------------------------------------------
/// Checking two vertices of a graph is connected or not
/// @param i, j: two vertices
/// @param adj; the adjacency list of the graph
/// @return true: if (i, j) is an edge
///         false: otherwise
//--------------------------------------------------------------------
bool isConnected(int i, int j, set<int> *adj) {
  int i_temp, j_temp;
  if (adj[i].size() < adj[j].size()) {
	i_temp = i;
	j_temp = j;
  }
  else {
	i_temp = j;
	j_temp = i;
  }
  set<int>::const_iterator it;
  for (it = adj[i_temp].begin(); it != adj[i_temp].end(); it++) {
	if (*it == j_temp) {
	  return true;
	}
  }
  return false;
}

//--------------------------------------------------------------------
/// Coloring Algorithm
/// @param R: the vertices needed to color
/// @param adj: the adjacency list of the graph
/// @param Q, QMAX: store a clique and current maximum clique
/// @reference: http://insilab.org/maxclique
//--------------------------------------------------------------------
void coloring(vertices &R, set<int> *adj, list<int> Q, list<int> QMAX) {
  list<int> colors[R.size()+1];

  int j = 0;
  int maxno = 1;
  int min_k = QMAX.size() - Q.size() + 1;
  int k = 1;
  list<int>::const_iterator it;
  for (int i = 0; i < R.size(); i++) {
    int pi = R[i].second;
    k = 1;
    while (true) {
	  bool selected = false;
	  for (it = colors[k].begin(); it != colors[k].end(); ++it) {
		if (isConnected(pi, *it, adj)) {
		  selected = true;
		  break;
		}
	  }
	  if (selected)
		k++;
	  else
		break;
	}
    if (k > maxno) {
      maxno = k;
    }

    colors[k].push_back(pi);
    if (k < min_k) {
      R[j++].second = pi;
    }
  }
  if (j > 0) R[j-1].first = 0;
  if (min_k <= 0) min_k = 1;
  for (k = min_k; k <= maxno; k++) {
    for (it = colors[k].begin(); it != colors[k].end(); ++it) {
      R[j].second = *it;
	  R[j++].first = k;
    }
  }
}

//--------------------------------------------------------------------
/// Recursively expand clique
/// @param R: the vertices of the graph
/// @param adj: the adjacency list of the graph
/// @param Q, QMAX: store a clique and current maximum clique
/// @reference: http://insilab.org/maxclique
//--------------------------------------------------------------------
void expand_clique(vertices R, set<int> *adj, list<int> &Q, list<int> &QMAX) {
  while (R.size()) {
    if (Q.size() + R[R.size()-1].first > QMAX.size()) {
      Q.push_back(R[R.size()-1].second);
      vertices Rp;
      for (vertices::iterator it = R.begin() ; it != R.end(); ++it)
		if (isConnected((*it).second, R[R.size()-1].second, adj))
		  Rp.push_back(make_pair(0, (*it).second));
      if (Rp.size()) {
        coloring (Rp, adj, Q, QMAX);
        expand_clique(Rp, adj, Q, QMAX);
      }
      else if (Q.size() > QMAX.size()) { 
		QMAX.assign(Q.begin(), Q.end());
      }
	  Rp.clear();
      Q.pop_back();
    }
    else {
      return;
    }
    R.pop_back();
  }
}

//--------------------------------------------------------------------
/// Maximum Clique Algorithm
/// @param n: the number of vertices of the graph
/// @param adj: the adjacency list of the graph
/// @return msize: the size of the maximum clique
/// @reference: http://insilab.org/maxclique
//--------------------------------------------------------------------
int maximum_clique(int n, set<int> *adj) {
  list<int> Q, QMAX;

  // create list of vertex (degree, label) then sort by degree
  vertices V;
  for (int i = 0; i < n; ++i)
	V.push_back(make_pair(adj[i].size(), i));
  std::sort(V.begin(), V.end(), std::greater<pair<int,int> >());

  // init color of each vertex
  int max_degree = V[0].first;
  for (int i = 0; i < max_degree; i++)
    V[i].first = i + 1;
  for (int i = max_degree; i < n; i++)
    V[i].first = max_degree + 1;

  // recursively find maximum clique
  expand_clique(V, adj, Q, QMAX);
  return QMAX.size();
}
#endif

//--------------------------------------------------------------------
/// Generate Barabasi-Albert model graph
/// and calculate the size of maximum clique
/// @param n, d: parameters of Random Graph
/// @param increment: frequency of running maxclique algorithm
//--------------------------------------------------------------------
int *barabasi_albert_model(int n, int d, int increment) {
  int *result = new int[n/increment];
  int counter = 0;

#ifdef ARRAY
  bool **conn = new bool *[n];
  for (int i = 0; i < n; i++) {
	conn[i] = new bool[n];
	for (int j = 0; j < n; j++)
	  conn[i][j] = false;
  }
#else
  set<int> *adjacent = new set<int>[n];
#endif

  int *fenwick_array = new int[n]; // store Fenwick Tree 
  int *degrees = new int[n]; // store degree of each vertex
  bool *is_selected = new bool[n];
  for (int i = 0; i < n; i++)
	is_selected[i] = false;

  int *selected_vertice = new int[d];

  for (int i = 0; i < d; i++) {
	degrees[i] = 1;
	Fen_add_element(fenwick_array, i, 1);

#ifdef ARRAY
    conn[d][i] = true;
	conn[i][d] = true;
	for (int j = 0; j < i; j++) {
	  conn[i][j] = true;
	  conn[j][i] = true;
	}
#else
    adjacent[d].insert(i);
    adjacent[i].insert(d);
	for (int j = 0; j < i; j++) {
	  adjacent[i].insert(j);
	  adjacent[j].insert(i);
	}
#endif
  }
  degrees[d] = d;
  Fen_add_element(fenwick_array, d, d);

  int num_links = d;
  for (int i = d+1; i < n; i++) {
	int range = 2*num_links;

	// link new node to d old nodes
	for (int j = 0; j < d; j++) {
	  int random = rand() % range;
#if defined DEBUG
	  cout << "Random = " << random << endl;
#endif
	  int selected_vertex;
	  /*
	  int cumulative = 0;
	  for (selected_vertex = 0; selected_vertex < i; selected_vertex++) {
		if (is_selected[ selected_vertex ])
		  continue;
		cumulative += degrees[ selected_vertex ];
		if (random < cumulative)
		  break;
	  }
	  */
	  // using Fenwick Tree to search for selected_vertex
	  int min = 0;
	  int max = i;
	  while (min <= max) {
		selected_vertex = (min + max) / 2;
		int temp1 = Fen_sum(fenwick_array, selected_vertex-1);
		int temp2 = Fen_sum(fenwick_array, selected_vertex);
		if (random >= temp1 && random < temp2)
		  break;
		else if (random >= temp2)
		  min = selected_vertex + 1;
		else // if (random < temp1)
		  max = selected_vertex - 1;
	  }
#if defined DEBUG
	  cout << "Selected_Vertex = " << selected_vertex << endl;
#endif
	  
	  selected_vertice[j] = selected_vertex;
	  is_selected[ selected_vertex ] = true;
	  
	  range -= degrees[ selected_vertex ];
	  Fen_add(fenwick_array, i, -degrees[ selected_vertex ], selected_vertex);
	}
	
	// update degree of selected vertice
	for (int j = 0; j < d; j++) {
	  degrees[ selected_vertice[j] ]++;
	  is_selected[ selected_vertice[j] ] = false;

	  Fen_add(fenwick_array, i, degrees[ selected_vertice[j] ], selected_vertice[j]);

#ifdef ARRAY
	  conn[ selected_vertice[j] ][i] = true;
	  conn[i][ selected_vertice[j] ] = true;

	  for (int k = 0; k < j; k++) {
		conn[selected_vertice[j]][ selected_vertice[k] ] = true;
		conn[selected_vertice[k]][ selected_vertice[j] ] = true;
	  }
#else
	  adjacent[ selected_vertice[j] ].insert(i);
	  adjacent[i].insert(selected_vertice[j]);

	  for (int k = 0; k < j; k++) {
		adjacent[selected_vertice[j]].insert(selected_vertice[k]);
		adjacent[selected_vertice[k]].insert(selected_vertice[j]);
	  }
#endif
	}
	degrees[i] = d;
	Fen_add_element(fenwick_array, i, d);

	num_links += d;

	// run maximum clique algorithm
	if ((i+1) % increment == 0) {
#ifdef ARRAY
	  Maxclique m(conn, i+1);
	  int *qmax, qsize;
	  m.mcqdyn(qmax, qsize);
	  result[ counter++ ] = qsize;
	  delete [] qmax;
#else
	  result[ counter++ ] = maximum_clique(i+1, adjacent);
#endif
	}
  }

  delete [] fenwick_array;
  delete [] degrees;
  delete [] selected_vertice;
  delete [] is_selected;
#ifdef ARRAY
  for (int i = 0; i < n; i++)
	delete [] conn[i];
  delete conn;
#else
  delete [] adjacent;
#endif
  return result;
}

int getOutputFileNumber(int n, int d) {
  DIR *dir;
  dirent *pdir;
  int t_max = 0;

  dir = opendir(("./" + output_folder).c_str());
  int n_temp, d_temp, t_temp;
  string filename_temp;
  while (pdir = readdir(dir)) {
	filename_temp = pdir->d_name;
	n_temp = d_temp = t_temp = 0;
#ifdef ARRAY
	sscanf(filename_temp.c_str(), "n%d_d%d_array_%d", &n_temp, &d_temp, &t_temp);
#else
	sscanf(filename_temp.c_str(), "n%d_d%d_list_%d", &n_temp, &d_temp, &t_temp);
#endif
	if (n == n_temp && d == d_temp)
	  if (t_temp > t_max) {
		t_max = t_temp;
	  }
  }
  return t_max+1;
}

//--------------------------------------------------------------------
/// Process parameters
/// @param argc, argv: input parameters
/// @param n, d: parameters of Random Graph
/// @param t: the number of tries
/// @return true if successful
///         false otherwise
//--------------------------------------------------------------------
bool processInput(int argc, char *argv[], int &n, int &d,
				  int &increment, int &t) {
  if (argc >= 4) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return false;
    istringstream iss2( argv[2] );
    if (!(iss2 >> d))
      return false;
    istringstream iss3( argv[3] );
    if (!(iss3 >> increment))
      return false;
  }
  else {
	return false;
  }
  if (argc >= 5) {
	istringstream iss4( argv[4] );
    if (!(iss4 >> t))
	  return false;
  }
  return true;
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  int n, d, increment, tries = 1000;
  // process input
  if (processInput(argc, argv, n, d, increment, tries) == false)
	return 0;
  
  int fileNumber = getOutputFileNumber(n, d);
  fstream fs;
  char filename[128];
#ifdef ARRAY
  snprintf(filename, sizeof(filename), "n%d_d%d_array_%d.txt", n, d, fileNumber);
#else
  snprintf(filename, sizeof(filename), "n%d_d%d_list_%d.txt", n, d, fileNumber);
#endif
  fs.open((output_folder + "/" + filename).c_str(), fstream::out | fstream::app);
  
  for (int i = 0; i < tries; i++) {
	int *result = barabasi_albert_model(n, d, increment);
	for (int j = 0; j < n/increment; j++)
	  fs << result[j] << " ";
	fs << endl;
	delete [] result;
  }

  fs.close();
  return 0;
}
